
active();

$("#cpf").mask('000.000.000-00')

function active(){
    
    $.ajax({
        method: 'GET',
        url: 'https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome',
        success: function(result){
            console.log(result)
            result.forEach(estado => {
                $("#estado").append(`<option value=${estado.id}>${estado.nome}</option>`);
            });
        },
    });

    $("#estado").change(function(){
        $("#cidade").empty();
        getCidades($("#estado").find(":selected").val())
    })

    $("#incluir").click(function(){
            $("#tbody").append(`
            <tr>
                <td>${$("#nome").val()}</td>
                <td>${$("#cpf").val()}</td>
                <td>${$("#data-nascimento").val()}</td>
                <td>${$("#estado").val()}</td>
                <td>${$("#cidade").val()}</td>
                <td><img class="table-img" src="./img/editar_icon.svg" alt=""></td>
                <td><img class="table-img" src="./img/excluir_icon.svg" alt=""></td>
            </tr>
        `)
    })

    return
}


function getCidades(estadoId){

    $.ajax({
        method: 'GET',
        url: 'https://servicodados.ibge.gov.br/api/v1/localidades/estados/' + estadoId + '/municipios',
        success: function(result){
            result.forEach(cidade => {
                $("#cidade").append(`<option value=${cidade.id}>${cidade.nome}</option>`);
            });
        },
    });
}